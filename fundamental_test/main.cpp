
#include "opencv2/calib3d/calib3d.hpp"

#include <vector>
#include <string>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <fstream>

using namespace cv;
using namespace std;

void find_fundamental(const vector<Point2f> &c0, const vector<Point2f> &c1, double focal0, double focal1)
{
    Mat F = findFundamentalMat(Mat(c0), Mat(c1), FM_8POINT, 0, 0);
    std::cout << "F:" << std::endl;
    std::cout << F << std::endl;

    Mat proj0 = Mat::eye(3, 3, CV_64F);
    Mat proj1 = Mat::eye(3, 3, CV_64F);
    proj0.at<double>(0,0) = proj0.at<double>(1, 1) = focal0;
    proj1.at<double>(0,0) = proj1.at<double>(1, 1) = focal1;
    Mat E = proj1.t() * F * proj0;
    Mat R1, R2, t;
    decomposeEssentialMat(E, R1, R2, t);

    std::cout << "E:" << std::endl;
    std::cout << E << std::endl;

    std::cout << "R1:" << std::endl;
    std::cout << R1 << std::endl;

    std::cout << "R2:" << std::endl;
    std::cout << R2 << std::endl;

    std::cout << "t:" << std::endl;
    std::cout << t << std::endl;
}

vector<Point2f> loadPoints(const char *filename)
{
    ifstream ifile(filename);
    vector<Point2f> points;
    while(ifile)
    {
        Point2f p;
        ifile >> p.x >> p.y;
        if (!ifile.eof())
        {
            points.push_back(p);
        }
    }

    return points;
}

int main(int argc, char** argv)
{
    if (argc <= 2)
    {
        return -1;
    }

    vector<Point2f> c0 = loadPoints(argv[1]);
    vector<Point2f> c1 = loadPoints(argv[2]);
    find_fundamental(c0, c1, 1.7320508075688774, 1.753287525081759);
    return 0;
}
